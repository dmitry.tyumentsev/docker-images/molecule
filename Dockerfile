FROM python:3.11-alpine3.18 AS builder

COPY requirements.txt requirements.txt

RUN apk add --no-cache gcc musl-dev

RUN pip wheel --no-cache-dir --wheel-dir /usr/src/ansible/wheels -r requirements.txt


FROM docker:24.0.5-cli-alpine3.18

RUN apk add --no-cache py3-pip python3 git

WORKDIR /ansible

COPY --from=builder /usr/src/ansible/wheels /wheels

RUN pip install --no-cache-dir /wheels/*

RUN rm -rf /wheels/

ENV PYTHONUNBUFFERED=1
ENV PY_COLORS=1
ENV ANSIBLE_FORCE_COLOR=1
